#!/usr/bin/env python
#### change directory to import instance_occlsegm ##
import sys
sys.path.insert(1, '/home/student2/PycharmProjects/masterproject/relook/jsk_apc/demos/instance_occlsegm')
#### change directory to import instance_occlsegm ##

import argparse

from instance_occlsegm_lib.contrib import instance_occlsegm


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--split',
        choices=['train', 'test'],
        default='train',
        help='dataset split',
    )
    parser.add_argument(
        '--augmentation',
        action='store_true',
        help='do augmentation',
    )
    args = parser.parse_args()

    data = instance_occlsegm.datasets.PanopticOcclusionSegmentationDataset(
        args.split, augmentation=args.augmentation
    )
    instance_occlsegm.datasets.view_panoptic_occlusion_segmentation_dataset(
        data
    )
