#!/usr/bin/env python

#### change directory to import instance_occlsegm ##
import sys
sys.path.insert(1, '/home/student2/PycharmProjects/masterproject/relook/jsk_apc/demos/instance_occlsegm')
#### change directory to import instance_occlsegm ##

import argparse

# import instance_occlsegm_lib
from instance_occlsegm_lib.contrib import synthetic2d
from instance_occlsegm_lib import datasets


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--split', default='train', choices=['train', 'test'], help='split'
    )
    parser.add_argument('--aug', action='store_true', help='data aug')
    args = parser.parse_args()
    print('Args:', args)

    dataset = synthetic2d.datasets.ARC2017OcclusionDataset(
        split=args.split, do_aug=args.aug
    )
    datasets.view_instance_seg_dataset(
        dataset, n_mask_class=3)
